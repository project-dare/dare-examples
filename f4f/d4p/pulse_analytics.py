import json

import numpy as np
from dispel4py.core import GenericPE
from dispel4py.workflow_graph import WorkflowGraph
from scipy import stats


class ProducerPE(GenericPE):

    def __init__(self):
        GenericPE.__init__(self)
        self._add_input('input')
        self._add_output('output')

    def _process(self, data):
        data = data["data"]
        varialbes_descr = {}
        for variable, values in data.items():
            time_values = np.array(self.update_str_values(values))
            descr = stats.describe(time_values)
            varialbes_descr[variable] = {
                "nobs": str(descr.nobs),
                "minmax": str(descr.minmax),
                "mean": str(descr.mean),
                "variance": str(descr.variance),
                "skewness": str(descr.skewness),
                "kurtosis": str(descr.kurtosis)
            }
        # filename = "analytics.json"
        # with open(filename, "w") as f:
        #     f.write(json.dumps(varialbes_descr))
        # self.write("output", varialbes_descr, location=filename, metadata={'results': varialbes_descr})
        self.write('output', varialbes_descr)
        # return {"output": varialbes_descr}

    @staticmethod
    def update_str_values(values):
        new_values = []
        for value in values:
            if not value or value == "":
                new_values.append(0)
            elif type(value) == str:
                new_values.append(int(value))
            else:
                new_values.append(value)
        return new_values


class PulseAnalyticsPE(GenericPE):

    def __init__(self):
        GenericPE.__init__(self)
        self._add_input('input')
        self._add_output('output')

    def _process(self, data):
        filename = "analytics.json"

        print(data["input"])
        with open(filename, "w") as f:
            f.write(json.dumps(data["input"]))
        # self.write("output", data["input"], location=filename, metadata={'results': data["input"]})


graph = WorkflowGraph()
producer = ProducerPE()
producer.name = 'producer'
analytics = PulseAnalyticsPE()
graph.connect(producer, 'output', analytics, 'input')
