#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
baseCommand: [python]
inputs:
  - id: script
    type: File
    inputBinding:
      position: 1
outputs: []