import json
from os import environ
from os.path import join

import numpy as np
from scipy import stats


def get_analytics():
    data = json.loads(environ["INPUT_DATA"])
    varialbes_descr = {}
    for variable, values in data.items():
        time_values = np.array(update_str_values(values))
        descr = stats.describe(time_values)
        varialbes_descr[variable] = {
            "nobs": str(descr.nobs),
            "minmax": str(descr.minmax),
            "mean": str(descr.mean),
            "variance": str(descr.variance),
            "skewness": str(descr.skewness),
            "kurtosis": str(descr.kurtosis)
        }
    return varialbes_descr


def update_str_values(values):
    new_values = []
    for value in values:
        if not value or value == "":
            new_values.append(0)
        elif type(value) == str:
            new_values.append(int(value))
        else:
            new_values.append(value)
    return new_values


def write_to_file(data):
    path = "/home/mpiuser/sfs/{}/runs/{}/analytics".format(environ["USERNAME"], environ["RUN_DIR"])
    filename = "analytics.json"
    with open(join(path, filename), "w") as f:
        f.write(json.dumps(data))


if __name__ == '__main__':
    analytics = get_analytics()
    write_to_file(analytics)
