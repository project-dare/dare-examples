import requests
import sys
import os

OUTPUT = os.environ['OUTPUT']
PREV_RUN_ID = os.environ['PREV_RUN_ID']

# Download with progress 
link = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/dispel4py_RAmapping.py'
file_name = './modified_scripts/dispel4py_RAmapping.py'
with open(file_name, 'wb') as f:
        print('Downloading %s' % file_name)
        response = requests.get(link, stream=True)
        total_length = response.headers.get('content-length')

        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                done = int(50 * dl / total_length)
                sys.stdout.write('\r[%s%s]' % ('=' * done, ' ' * (50-done)) )    
                sys.stdout.flush()

# Append definition of enviromental variables
lines = open(file_name).readlines()
_lines = []
for line in lines:
    if 'import os' in line:
        _lines.append(line)
        _lines.append("os.environ['OUTPUT'] = "+OUTPUT+"\n")
        _lines.append("os.environ['REPOS_URL'] = 'http://'+os.getenv('SPROV_SERVICE_HOST')+':'+os.getenv('SPROV_SERVICE_PORT')+'/workflowexecutions/insert'\n")
        _lines.append("os.environ['RAMAP_RUNID'] = os.environ['RUN_ID']\n")
        _lines.append("os.environ['PGM_RUNID'] = "+PREV_RUN_ID+"\n")
    else:
        _lines.append(line)
with open(file_name, 'w') as f:
    f.writelines(_lines)

