import requests
import sys
import os

STAGED_DATA = os.environ['STAGED_DATA']
OUTPUT = os.environ['OUTPUT']
PREV_RUN_ID = os.environ['PREV_RUN_ID']

# Download with progress 
link = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/dispel4py_RA.pgm_story.py'
file_name = './modified_scripts/dispel4py_RA.pgm_story.py'
with open(file_name, 'wb') as f:
        print('Downloading %s' % file_name)
        response = requests.get(link, stream=True)
        total_length = response.headers.get('content-length')

        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                done = int(50 * dl / total_length)
                sys.stdout.write('\r[%s%s]' % ('=' * done, ' ' * (50-done)) )    
                sys.stdout.flush()

# Append definition of enviromental variables
lines = open(file_name).readlines()
_lines = []
for line in lines:
    if 'import os' in line:
        _lines.append(line)
        _lines.append("os.system('wget -P /home/mpiuser/sfs/d4p https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/seismo.py')\n")
        _lines.append("sys.path.append(os.environ['PWD'])\nos.environ['STAGED_DATA'] = "+STAGED_DATA+"\n")
        _lines.append("try:\n    os.mkdir("+OUTPUT+")\nexcept:\n    pass\n")
        _lines.append("os.environ['OUTPUT'] = "+OUTPUT+"\n")
        _lines.append("os.environ['REPOS_URL'] = 'http://'+os.getenv('SPROV_SERVICE_HOST')+':'+os.getenv('SPROV_SERVICE_PORT')+'/workflowexecutions/insert'\n")
        _lines.append("os.environ['PGM_RUNID'] = os.environ['RUN_ID']\n")
        _lines.append("os.environ['PREPOC_RUNID'] = "+PREV_RUN_ID+"\n")
    else:
        _lines.append(line)
with open(file_name, 'w') as f:
    f.writelines(_lines)

