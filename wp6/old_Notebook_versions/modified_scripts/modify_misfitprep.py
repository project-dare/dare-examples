import requests
import sys
import os

STAGED_DATA = os.environ['STAGED_DATA']
PREP_IN_DATA = os.environ['PREP_IN_DATA'] #fm
PREV_RUN_ID = os.environ['PREV_RUN_ID']
MISFIT_PREP_CONFIG = os.environ['MISFIT_PREP_CONFIG']
OUTPUT = os.environ['OUTPUT']

# Download with progress 
link = 'https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/create_misfit_prep.py'
file_name = './modified_scripts/create_misfit_prep.py'
with open(file_name, 'wb') as f:
        print('Downloading %s' % file_name)
        response = requests.get(link, stream=True)
        total_length = response.headers.get('content-length')

        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = int(total_length)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                f.write(data)
                done = int(50 * dl / total_length)
                sys.stdout.write('\r[%s%s]' % ('=' * done, ' ' * (50-done)) )    
                sys.stdout.flush()

# Append definition of enviromental variables
lines = open(file_name).readlines()
_lines = []
for line in lines:
    if 'import sys' in line:
        _lines.append(line)
        _lines.append("os.system('wget -P /home/mpiuser/sfs/d4p https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/preprocessing_functions.py')\n")
        _lines.append("os.system('wget -P /home/mpiuser/sfs/d4p https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/processing.json')\n")
        _lines.append("os.system('wget -P /home/mpiuser/sfs/d4p https://gitlab.com/project-dare/WP6_EPOS/raw/RA_total_script/processing_elements/Download_Specfem3d_Misfit_RA/seismo.py')\n")
        _lines.append("os.system('wget -P /home/mpiuser/sfs/d4p https://gitlab.com/project-dare/dare-api/tree/master/examples/wp6/wp6_inputfiles/events_info.xml')\n") #fm
        _lines.append("sys.path.append(os.environ['PWD'])\nos.environ['STAGED_DATA'] = "+STAGED_DATA+"\n")
        _lines.append("os.environ['DOWNL_RUNID'] = "+PREV_RUN_ID+"\nos.environ['PREPOC_RUNID'] = os.environ['RUN_ID']\n")
        _lines.append("os.environ['REPOS_URL'] = 'http://'+os.getenv('SPROV_SERVICE_HOST')+':'+os.getenv('SPROV_SERVICE_PORT')+'/workflowexecutions/insert'\n")
        _lines.append("os.environ['MISFIT_PREP_CONFIG'] = "+MISFIT_PREP_CONFIG+"\n")
        _lines.append("os.environ['PREP_IN_DATA'] = "+PREP_IN_DATA+"\n") #fm
        _lines.append("try:\n    os.mkdir("+OUTPUT+")\nexcept:\n    pass\n")
    else:
        _lines.append(line)
with open(file_name, 'w') as f:
    f.writelines(_lines)

