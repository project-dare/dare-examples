import json
import time
from os import getcwd
from os.path import join, exists

import requests
import wget
import yaml
from IPython.display import clear_output


# ********************************** Login & Init *************************************************
def read_credentials():
    """
    Reads the user's credentials and the issuer of the token. It requires a yaml file with the credentials to be
    present. We provide the file example_credentials.yaml with the fields that should be provided.
    You can copy the file and create one named credentials.yaml.
    """
    cred_file = join(getcwd(), "credentials.yaml")
    example_cred_file = join(getcwd(), "example_credentials.yaml")
    cred_filename = cred_file if exists(cred_file) else example_cred_file
    with open(cred_filename, "r") as f:
        return yaml.safe_load(f)


def login(hostname, username, password, requested_issuer):
    """
    Function that uses the dare-login component in order to sign in the user to the dare-platform

    Args
        | hostname (str): the base URL to the dare-login component (e.g. https://testbed.project-dare.eu/dare-login)
        | username (str): the username in the credentials yaml file
        | password (str): the password in the credentials yaml file
        | requested_issuer (str): the issuer in the credentials yaml file

    Returns
        dict: dictionary with the access and refresh tokens
    """
    data = {
        "username": username,
        "password": password,
        "requested_issuer": requested_issuer
    }
    headers = {"Content-Type": "application/json"}
    r = requests.post(hostname + '/auth', data=json.dumps(data), headers=headers)
    if r.status_code == 200:
        response = json.loads(r.text)
        return {"access_token": response["access_token"], "refresh_token": response["refresh_token"]}
    else:
        print("Could not authenticate user!")


# ******************************** Workspaces ********************************
# Get workspace url by name
def get_workspace(name, hostname, token):
    """
    Uses the Dispel4py Information Registry to find a workspace by name

    Args
        | name (str): the name of the workspace
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
    """
    # Get json response
    req = requests.get(hostname + '/workspaces/',
                       params={"access_token": token, "name": name})
    resp_json = json.loads(req.text)
    if type(resp_json) == dict:
        return resp_json["url"], resp_json["id"]
    elif type(resp_json) == list:
        # Iterate and retrieve
        return ([i['url'] for i in resp_json if i['name'] == name][0],
                [i['id'] for i in resp_json if i['name'] == name][0])


# Create workspace using d4p registry api
def create_workspace(clone, name, desc, hostname, token):
    """
    Function that uses the Dispel4py Information Registry to create a new workspace

    Args
        | clone (str): defines if the workspace will be a clone of some existing workspace
        | name (str): the name of the workspace
        | desc (str): a description for the workspace
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function

    """
    # Prepare data for posting
    data = {
        "clone_of": clone,
        "name": name,
        "access_token": token,
        "description": desc
    }
    # Request for d4p registry api
    _r = requests.post(hostname + '/workspaces/', data=data)

    # Progress check
    if _r.status_code == 201:
        print('Added workspace: ' + name)
        return get_workspace(name, hostname, token)
    else:
        print('Add workspace resource returns status_code: ' +
              str(_r.status_code))
        return get_workspace(name, hostname, token)


def delete_workspace(name, hostname, token):
    """
    Function that uses the Dispel4py Information Registry to delete an existing workspace

    Args
        | name (str): the name of the workspace
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
    """
    workspace_url, wid = get_workspace(name, hostname, token)
    _r = requests.delete(hostname + '/workspaces/' + str(wid) + '/',
                         data={"access_token": token})
    # Progress check
    if _r.status_code == 204:
        print('Deleted workspace ' + name)
    else:
        print('Delete workspace returned status code: ' + str(_r.status_code))
        print(_r.text)


# ******************************* PEs & PEImpls ****************************************
# Create ProcessingElement using d4p registry api
def create_pe(desc, name, conn, pckg, workspace, clone, peimpls, hostname, token):
    """
    Uses the Dispel4py Information Registry to create a new PE

    Args
        | desc (str): a description for the PE
        | name (str): the name of the PE
        | pckg (str): the package name
        | workspace (str): the name of the associated workspace
        | clone (str): defines if it's a clone of an existing PE
        | peimpls (list): provides the peimpls
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
    """
    assert isinstance(conn, list)
    assert isinstance(peimpls, list)

    # Prepare data for posting
    data = {
        "description": desc,
        "name": name,
        "connections": conn,
        "pckg": pckg,
        "workspace": workspace,
        "clone_of": clone,
        "access_token": token,
        "peimpls": peimpls
    }
    # Request for d4p registry api
    _r = requests.post(hostname + '/pes/', data=data)
    # Progress check
    if _r.status_code == 201:
        print('Added Processing Element: ' + name)
        return json.loads(_r.text)['url']
    else:
        print('Add Processing Element resource returns status_code: ' +
              str(_r.status_code))


# Create ProcessingElement Implementation using d4p registry api
def create_peimpl(desc, code, parent_sig, pckg, name, workspace, clone, hostname, token):
    """
    Creates a new PEImpl using the Dispel4py Information Registry

    Args
        | desc (str): the description of the PE
        | code (str): the code of the workflow
        | parent_sig (str): the PE signature
        | pckg (str): the package name
        | name (str): the PE name
        | workspace (str): the workspace name
        | clone (str): defines if it's a PEImpl clone
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
    """
    # Prepare data for posting
    data = {
        "description": desc,
        "access_token": token,
        "code": code,
        "parent_sig": parent_sig,
        "pckg": pckg,
        "name": name,
        "workspace": workspace,
        "clone_of": clone
    }
    # Request for d4p registry api, verify=False is only for demo purposes
    # nginx is too slow on response, open issue
    _r = requests.post(hostname + '/peimpls/', data=data, verify=False)
    # Progress check
    if _r.status_code == 201:
        print('Added Processing Element Implementation: ' + name)
        return json.loads(_r.text)['id']
    else:
        print('Add Processing Element Implementation resource returns \
                status_code: ' + str(_r.status_code))


# *********************************  Workflow Registry ******************************************
# ***************************** DockerEnv ************************************
def create_docker_env_with_scripts(hostname, token, docker_name, docker_tag, script_names, path_to_files):
    """
        Register an entire docker environment to the DARE platform. Under the path_to_files directory you should already
        have stored a Dockerfile and the relevant bash or python scripts

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_names (list): a list of names for your docker scripts
            | path_to_files (str): the path to the Dockerfile and scripts

        Returns
            tuple: the response status code and the response content
    """
    if not exists(join(path_to_files, "Dockerfile")):
        return "Dockerfile does not exist in folder {}".format(path_to_files)
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_names": script_names,
        "access_token": token
    }
    with open(join(path_to_files, "Dockerfile"), 'r') as d:
        dockerfile = d.read()
    files = {"dockerfile": dockerfile}
    if script_names:
        for script_name in script_names:
            with open(join(path_to_files, script_name), 'r') as s:
                script = s.read()
            files[script_name] = script
    data["files"] = files
    response = requests.post(hostname + "/docker/", json=data)
    return response.status_code, response.text


def create_docker_env(hostname, token, docker_name, docker_tag, docker_path):
    """
        Create a docker environment with a Dockerfile (without any other scripts)

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | docker_path (str): the path to the Dockerfile (e.g. /home/user/docker/Dockerfile)

        Returns
            tuple: the response status code and the response content
    """
    if not exists(docker_path):
        return "File does not exist"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    with open(docker_path, 'r') as df:
        docker_content = df.read()
    files = {"dockerfile": docker_content}
    data["files"] = files
    response = requests.post(hostname + "/docker/", json=data)
    return response.status_code, response.text


def update_docker(hostname, token, update, docker_name, docker_tag, path_to_docker=None):
    """
        Function to update an existing docker environment. You should provide a dict (parameter update) with the fields
        to be updated. Valid keys to the dict are: name, tag, url, dockerfile

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | update (dict): docker fileds to be updated in the registry
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | path_to_docker (str): the path to the Dockerfile (e.g. /home/user/docker/Dockerfile) - optional, should be
            | used only if you need to update the Dockerfile

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token,
        "update": update
    }
    if path_to_docker:
        with open(path_to_docker, "r") as df:
            docker_content = df.read()
        files = {"dockerfile": docker_content}
        data["files"] = files
    response = requests.post(hostname + "/docker/update_docker/", json=data)
    return response.status_code, response.text


def provide_url(hostname, token, docker_name, docker_tag, docker_url):
    """
        Function to update the URL to a public docker image for a specific docker environment. The docker env should
        already be available in the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | docker_url (str): the URL to the public docker image

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "docker_url": docker_url,
        "access_token": token
    }
    response = requests.post(hostname + "/docker/provide_url/", data=data)
    return response.status_code, response.text


def delete_docker(hostname, token, docker_name, docker_tag):
    """
        Function to delete a docker environment (Dockerfile and associated scripts)

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    response = requests.delete(hostname + "/docker/delete_docker/", data=data)
    return response.status_code, response.text


def get_docker_by_name_and_tag(hostname, token, docker_name, docker_tag):
    """
        Retrieve a docker environment by name and tag

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment

        Returns
            tuple: the response status code and the response content
    """
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    response = requests.get(hostname + "/docker/bynametag", params=data)
    return response.status_code, response.text


def get_docker_by_user(hostname, token, username=None):
    """
        Retrieve a docker env by user. You can specify a user (if it's not you) otherwise the system will identify you
        from the provided token.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | username (str): optional, the username associated with the docker

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/docker/byuser"
    data = {"access_token": token}
    if username:
        data["requested_user"] = username
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_docker(hostname, token, docker_name, docker_tag, local_path):
    """
        Function to download a zip file containing the Dockerfile and the relevant scripts

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/docker/download"
    params = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "access_token": token
    }
    try:
        myfile = requests.get(url, params=params)
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    except (FileNotFoundError, Exception) as e:
        return "An error occurred while downloading the file: {}".format(e)


# **************************** Docker Scripts **********************************
def add_script_to_existing_docker(hostname, token, docker_name, docker_tag, script_name, path_to_script):
    """
        Function to register a script (bash or python) to an existing registered docker environment

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script
            | path_to_script (str): path to the folder that the script is stored (e.g. /home/user/docker/)

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/add/"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    with open(join(path_to_script, script_name), "r") as s:
        script_content = s.read()
    files = {script_name: script_content}
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def edit_script_in_existing_docker(hostname, token, docker_name, docker_tag, script_name, path_to_script):
    """
        Function to update a script (bash or python) to an existing registered docker environment

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script
            | path_to_script (str): path to the folder that the script is stored (e.g. /home/user/docker/)

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/edit/"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    with open(join(path_to_script, script_name), "r") as s:
        script_content = s.read()
    files = {script_name: script_content}
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def delete_script_in_docker(hostname, token, docker_name, docker_tag, script_name):
    """
        Function to delete a script of a docker env

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script

        Returns
                tuple: the response status code and the response content
    """
    url = hostname + "/scripts/delete/"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_script_by_name(hostname, token, docker_name, docker_tag, script_name):
    """
        Function to get a script of a docker env by name

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/byname"
    data = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_script(hostname, token, docker_name, docker_tag, script_name, local_path):
    """
        Function to download a script of a docker env by name

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | docker_name (str): the name of your docker environment
            | docker_tag (str): the tag of your docker environment
            | script_name (str): the name of the script

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/scripts/download"
    params = {
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "script_name": script_name,
        "access_token": token
    }
    try:
        myfile = requests.get(url, params=params)
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    except (FileNotFoundError, Exception) as e:
        return "An error occurred while downloading the file: {}".format(e)


# ********************************* Workflows *****************************************
def create_workflow(hostname, token, workflow_name, workflow_version, spec_name, path_to_cwls,
                    docker_name, docker_tag, workflow_part_data=None):
    """
        Registers a CWL workflow and its parts (if provided) to the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | spec_name (str): the name of the spec yaml file
            | path_to_cwls (str): path to the folder where the CWL files are stored (locally)
            | docker_name (str): the name of the associated docker env
            | docker_tag (str): the tag of the associated docker env
            | workflow_part_data (dict): name,version and yaml file name of each of the CWL of class CommandLineTool

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "docker_name": docker_name,
        "docker_tag": docker_tag,
        "spec_file_name": spec_name,
        "workflow_part_data": workflow_part_data,
        "access_token": token
    }
    with open(join(path_to_cwls, workflow_name), "r") as wp:
        workflow_content = wp.read()
    with open(join(path_to_cwls, spec_name), "r") as sp:
        spec_content = sp.read()
    files = {"workflow_file": workflow_content, "spec_file": spec_content}
    if workflow_part_data:
        for workflow_part in workflow_part_data:
            with open(join(path_to_cwls, workflow_part["name"]), "r") as sc:
                script_content = sc.read()
            files[workflow_part["name"]] = script_content
            if "spec_name" in workflow_part.keys():
                with open(join(path_to_cwls, workflow_part["spec_name"]), "r") as sp:
                    spec_content = sp.read()
                files[workflow_part["spec_name"]] = spec_content
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def update_workflow(hostname, token, workflow_name, workflow_version, update, workflow_path=None, specpath=None):
    """
        Updates a CWL workflow to the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | update (dict): the workflow's fields to be updated. Valid fields are: name, version, spec_name
            | workflow_path (str): optional, the path to the CWL of class Workflow (only for file update)
            | specpath (str): the path to the yaml file, optional (only if you need to update the file)

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/update_workflow/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token,
        "update": update
    }
    if workflow_path:
        with open(workflow_path, "r") as f:
            file_content = f.read()
    if specpath:
        with open(specpath, "r") as sp:
            spec_content = sp.read()
    if file_content or spec_content:
        files = {}
        if file_content:
            files["workflow_file"] = file_content
        if spec_content:
            files["spec_file"] = spec_content
        data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def update_associated_docker(hostname, token, workflow_name, workflow_version, docker_name, docker_tag):
    """
        Function to update the associated docker environment for a specific workflow

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | docker_name (str): the name of the associated docker env
            | docker_tag (str): the tag of the associated docker env

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/update_docker"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token,
        "docker_name": docker_name,
        "docker_tag": docker_tag
    }
    response = requests.post(url, data=data)
    return response.status_code, response.text


def delete_workflow(hostname, token, workflow_name, workflow_version):
    """
        Deletes a CWL workflow and its parts (if provided) from the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/delete_workflow/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token
    }
    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_workflow_by_name_and_version(hostname, token, workflow_name, workflow_version):
    """
        Function to retrieve a CWL workflow by name and version

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/bynameversion"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token
    }
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_workflow(hostname, token, workflow_name, workflow_version, local_path, dockerized=None):
    """
        Function to download a CWL workflow (class Workflow) and its CWL parts (class CommandLineTool), the relevant
        yaml files (spec files) and, if dockerized is set to True, it also downloads the Dockerfile and scripts
        (bash or python) of the associated docker container

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | local_path (str): the path where the zip file will be stored
            | dockerized (bool): true to download the Dockerfile and scripts along with the workflow files. Leave it
            empty if you want only the CWL and yaml files

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflows/download"
    params = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "access_token": token
    }
    if dockerized:
        params["dockerized"] = True
    myfile = requests.get(url, params=params)
    if myfile.status_code == 200:
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    else:
        return myfile.status_code, myfile.text


# ******************************* Workflow Parts ********************************
def add_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                      path_to_scripts, spec_name=None):
    """
        Registers a CWL workflow part (class CommandLineTool) to the registry. Relevant yaml file can also be stored,
        if different yaml files are used.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be registered
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be registered
            | path_to_scripts (str): the path to the folder containing the CWL files
            | spec_name (str): optional, use it only if you have different yaml file for each CWL

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/add/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    with open(join(path_to_scripts, workflow_part_name), "r") as s:
        script_content = s.read()
    files = {workflow_part_name: script_content}
    if spec_name:
        data["spec_name"] = spec_name
        with open(join(path_to_scripts, spec_name), "r") as sp:
            spec = sp.read()
        files[spec_name] = spec
    data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def edit_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                       update, spec_name=None, path_to_files=None):
    """
        Updates a CWL workflow (class CommandLineTool) to the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be updated
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be updated
            | update (dict): the workflow's fields to be updated. Valid fields are: name, version, spec_name
            | spec_name (str): optional, use it only if you have different yaml file for each CWL
            | path_to_files (str): optional, use it only when you want to update the actual files and not only the
            workflow object's fields

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/edit/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token,
        "update": update
    }

    if path_to_files:
        with open(join(path_to_files, workflow_part_name), "r") as f:
            file_content = f.read()
        files = {workflow_part_name: file_content}
        if spec_name:
            data["spec_name"] = spec_name
            with open(join(path_to_files, spec_name), "r") as sp:
                spec_content = sp.read()
            files[spec_name] = spec_content
        data["files"] = files
    response = requests.post(url, json=data)
    return response.status_code, response.text


def delete_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version):
    """
        Deletes a CWL workflow (class CommandLineTool) from the registry.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be deleted
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be deleted

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/delete/"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    response = requests.delete(url, data=data)
    return response.status_code, response.text


def get_workflow_part_by_name_and_version(hostname, token, workflow_name, workflow_version, workflow_part_name,
                                          workflow_part_version):
    """
        Function to retrieve a CWL of class CommandLineTool by name, tag and CWL name & version of the CWL of class
        Workflow

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be retrieved
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be retrieved

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/bynameversion"
    data = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    response = requests.get(url, params=data)
    return response.status_code, response.text


def download_workflow_part(hostname, token, workflow_name, workflow_version, workflow_part_name, workflow_part_version,
                           local_path):
    """
        Downloads a CWL of class CommandLineTool and its associated yaml file if exists.

        Args
            | hostname (str): the base url to the DARE platform (e.g. https://testbed.project-dare.eu)
            | token (str): your authentication token which is returned from the login function
            | workflow_name (str): the name of the CWL with class Workflow
            | workflow_version (str): the version of the CWL with class Workflow
            | workflow_part_name (str): the name of the CWL of class CommandLineTool to be retrieved
            | workflow_part_version (str): the version of the CWL of class CommandLineTool to be retrieved
            | local_path (str): the path to store the zip file / should contain the zip filename as well

        Returns
            tuple: the response status code and the response content
    """
    url = hostname + "/workflow_parts/download"
    params = {
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "workflow_part_name": workflow_part_name,
        "workflow_part_version": workflow_part_version,
        "access_token": token
    }
    try:
        myfile = requests.get(url, params=params)
        with open(local_path, "wb") as f:
            f.write(myfile.content)
        return "File successfully downloaded in {}".format(local_path)
    except (FileNotFoundError, Exception) as e:
        return "An error occurred while downloading the file: {}".format(e)


# ********************************* Executions ******************************************
# Spawn mpi cluster and run dispel4py workflow
def submit_d4p(impl_id, pckg, workspace_id, pe_name, n_nodes, token, hostname, image=None,
               reqs=None, **kw):
    """
    Uses the Execution API to execute a dispel4py workflow

    Args
        | impl_id (int): the ID of the PEImpl to be executed
        | pckg (str): the name of the PEImpl's package
        | workspace_id (int): the ID of the relevant workspace
        | pe_name (str): the name of the PE
        | n_nodes (int): the number of the containers to be created
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
        | image (str): the tag of the image of the container. If it's not provided the latest tag is used. The base tag
        is the registry.gitlab.com/project-dare/dare-platform/exec-context-d4p and we add the tag at the end, e.g.
        registry.gitlab.com/project-dare/dare-platform/exec-context-d4p:latest
        | reqs (str): URL to a requirements txt file for the execution

    """
    # Prepare data for posting
    data = {
        "impl_id": impl_id,
        "pckg": pckg,
        "wrkspce_id": workspace_id,
        "name": pe_name,
        "n_nodes": n_nodes,
        "access_token": token,
        "reqs": reqs if not (reqs is None) else "None"
    }
    if image:
        data["image"] = image
    d4p_args = {}
    for k in kw:
        d4p_args[k] = kw.get(k)
    data['d4p_args'] = json.dumps(d4p_args)
    # Request for dare api
    _r = requests.post(hostname + '/run-d4p', data=data)
    # Progress check
    if _r.status_code == 200:
        print(_r.text)
    else:
        print('DARE api resource / d4p-mpi-spec returns status_code: \
                ' + str(_r.status_code))
        print(_r.text)


def debug_d4p(hostname, impl_id, pckg, workspace_id, pe_name, token, reqs=None, output_filename="output.txt",
              file_format="txt", **kw):
    # Prepare data for posting
    data = {
        "impl_id": impl_id,
        "pckg": pckg,
        "wrkspce_id": workspace_id,
        "n_nodes": 1,
        "name": pe_name,
        "access_token": token,
        "output_filename": output_filename,
        "output_file_format": file_format,
        "reqs": reqs if not (reqs is None) else "None"
    }
    d4p_args = {}
    for k in kw:
        d4p_args[k] = kw.get(k)
    data['d4p_args'] = json.dumps(d4p_args)
    r = requests.post(hostname + '/playground', data=data)
    if r.status_code == 200:
        response = json.loads(r.text)
        if response["logs"]:
            print("Logs:\n========================")
            for log in response["logs"]:
                print(log)
        if response["output"]:
            if file_format == "txt":
                print("Output content:\n==============================")
                for output in response["output"]:
                    print(output)
            elif file_format == "json":
                print(response["output"])
    else:
        print('Playground returns status_code: \
                ' + str(r.status_code))
        print(r.text)


def submit_specfem(n_nodes, folder_name, filename, token, hostname, image=None):
    """
        Uses the Execution API to execute Specfem3D

        Args
            | n_nodes (int): the number of the containers to be created
            | folder_name (str): the name of the folder in the uploads directory which contains the file with the
            configuration
            | filename (str): the name of the configuration file
            | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
            | token (str): the access token aquired from the login function
            | image (str): the tag of the image of the container. If it's not provided the latest tag is used. The base
            tag is the registry.gitlab.com/project-dare/dare-platform/exec-context-d4p and we add the tag at the end,
            e.g. registry.gitlab.com/project-dare/dare-platform/exec-context-specfem3d:latest
        """
    # Prepare data for posting
    data = {
        "n_nodes": n_nodes,
        "folder_name": folder_name,
        "filename": filename,
        "access_token": token
    }
    if image:
        data["image"] = image
    # Request for dare api
    _r = requests.post(hostname + '/run-specfem', data=data)
    # Progress check
    if _r.status_code == 200:
        print(_r.text)
    else:
        print('DARE api resource / d4p-mpi-spec returns status_code: \
                ' + str(_r.status_code))


def submit_cwl(hostname, token, workflow_name, workflow_version, input_data=None, nodes=None):
    """
        Uses the Execution API to execute a CWL workflow

        Args
            | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
            | token (str): the access token aquired from the login function
            | workflow_name (str): the name of the registered workflow to be executed
            | workflow_version (str): the version of the workflow to be executed
            | input_data (dict): a dictionary with the necessary input data
            | nodes (int): the number of the containers to be created
        """
    url = hostname + "/run-cwl"

    data = {
        "access_token": token,
        "workflow_name": workflow_name,
        "workflow_version": workflow_version
    }

    if input_data:
        data["input_data"] = json.dumps(input_data)
    if nodes:
        data["nodes"] = nodes

    response = requests.post(url, data=data)
    print("CWL execution returns status code {} and description {}".format(response.status_code, response.text))


# *************************** Monitor Executions **********************************
def my_pods(token, hostname):
    """
    Lists all the running containers for a specific user based on his/her token

    Args
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function

    Returns
        json: the response of the Execution API for the running containers
    """
    _r = requests.get(hostname + '/my-pods?access_token=' + token)
    return _r.text


def pod_pretty_print(_json):
    """
    Uses the response from function mypods to print them in a more user-friendly way
    """
    print('Running containers...')
    print('\n')
    for i in _json:
        print('Container name: ' + i['name'])
        print('Container status: ' + i['status'])
        print('\n')
    print('\n')


def monitor(token, hostname):
    """
    Monitor function that uses the mypods function to retrieve the running containers and the pod_pretty_print function
    to print them to the user.
    """
    while True:
        clear_output(wait=True)
        resp = my_pods(token=token, hostname=hostname)
        pod_pretty_print(json.loads(resp))
        if not json.loads(resp):
            break
        time.sleep(1)


# *********************************** Files ***********************************
def upload(token, path, local_path, hostname):
    """
    Uses the Execution API to upload a zip file to the DARE platform

    Args
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
        | path (str): the directory inside the uploads dir where the file should be stored
        | local_path (str): the local path where the zip file to be uploaded exists

    Returns
        json: the response of the Execution API
    """
    params = (
        ('dataset_name', 'N/A'),
        ('access_token', token),
        ('path', path),
    )
    files = {
        'file': (local_path, open(local_path, 'rb')),
    }
    _r = requests.post(hostname + '/upload', params=params, files=files)
    return _r.text


def list_folders(token, hostname, num_run_dirs=None):
    """
    Uses the Execution API to list the directories of a user

    Args
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function

    Returns
        json: the response of the API
    """
    _r = requests.get("{}/my-files?access_token={}&num_run_dirs={}".format(hostname, token,
                                                                           num_run_dirs)) if num_run_dirs else \
        requests.get("{}/my-files?access_token={}".format(hostname, token))
    return _r.text


def list_folder_files(token, path, hostname):
    """
    Function to list the files inside a specific directory (select one of the listed dirs from the list_folders function

    Args
        | path (str): the path to the requested folder
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function

    Returns
        json: the API response
    """
    _r = requests.get(hostname + '/list?path=' + path + "&access_token=" + token)
    return _r.text


def download(token, path, hostname, local_path):
    """
    Uses the Execution API to download a file from the platform

    Args
        | path (str): the path where the file is in the platform
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
        | local_path (str): the local path to store the downloaded file
    """
    url = hostname + '/download?access_token=' + token + '&path=' + path
    wget.download(url, out=local_path)
    return 'Dowloading....'


def get_file_content(token, hostname, run_dir, filename):
    url = hostname + "/file-content"
    params = {
        "access_token": token,
        "run_dir": run_dir,
        "filename": filename
    }
    response = requests.get(url, params=params)
    return response.status_code, response.text


# Spawn mpi cluster and run specfem workflow
def send2drop(token, path, hostname):
    """
    Uses the execution API to send files in B2Drop

    Args
        | path (str): path to file to be uploaded
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function

    Returns
        json: the API response
    """
    _r = requests.get(hostname + '/send2drop?access_token=' + token + '&path=' + path)
    return _r.text


def folders_pretty_print(_json, num_run_dirs=None):
    """
    Get the response from the list_folders function and prints the directories of a user in a more user-friendly format
    """
    print('Uploaded files......')
    print('\n')
    for i in _json['uploads']:
        print('Api Local path: ' + i['path'])
        print('Execution path: ' + i['exec_path'])
        print('\n')
    print('\n')
    print('Files generated from runs......')
    print('\n')
    if _json['run']:
        _json['run'] = sorted(_json['run'], key=lambda k: k['path'], reverse=True)
        if num_run_dirs and len(_json['run']) > num_run_dirs:
            _json['run'] = _json['run'][:num_run_dirs]
    for i in _json['run']:
        print('Api Local path: ' + i['path'])
        print('Execution path: ' + i['exec_path'])
        print('\n')
    print('\n')
    print("Files generated from trial runs.......")
    print('\n')
    for i in _json['debug']:
        print('Api Local path: ' + i['path'])
        print('Execution path: ' + i['exec_path'])
        print('\n')


def files_pretty_print(_json):
    """
    Gets the response from the function list_folder_files and presents the result in a more user-friendly format
    """
    print('Listing files......')
    print('\n')
    for i in _json['files']:
        print('Api Local path: ' + i['path'].split('/')[-1])
        print('\n')
    print('\n')


def cleanup_dirs(hostname, token, uploads=False, runs=False):
    """
    Uses the Execution API to clean up a user's directories (e.g. uploads, runs etc)

    Args
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
        | uploads (bool): True/False if the uploads should or not be cleaned
        | runs (bool): True/False if the runs folder should or not be cleaned
    """
    url = hostname + "/cleanup"
    headers = {"Content-Type": "application/json"}
    data = {
        "access_token": token
    }
    if uploads:
        data["uploads"] = "uploads"
    if runs:
        data["runs"] = "runs"
    response = requests.get(url, params=data, headers=headers)
    return response.status_code, response.text
